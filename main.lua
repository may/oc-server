local internet = require("internet")
local json = require("json")
local component = require("component")

local function post_request(url, data)
	local handle = internet.request(url, json.encode(data))
	local text = ""
	for chunk in handle do
		text = text .. chunk
	end
	return text
end

local function get_me_contents()
	local items = component.me_controller.getItemsInNetwork()
	local length = items.n
	local result = {}
	for i = 1, length do
		local item = items[i]
		table.insert(result, item)
	end
	return result
end

local items = get_me_contents()
local response = post_request("https://httpbin.org/post", items)
print(response)
